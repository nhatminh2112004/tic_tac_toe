from tabulate import tabulate
import os
class TicTacToe(object):
    def __init__(self, n):
        self.number = n
        self.board = []
        for _ in range(self.number):
            self.board.append([])
        for i in self.board:
            for _ in range(self.number):
                i.append(" ")
    def draw_board(self):
        print(tabulate(self.board, tablefmt="psql"))
        # for i in self.board:
        #     for x in i:
        #         print(x,end = "")
        #     print()
    def row_win(self,player):
        for x in range(self.number):
            win = True
            for y in range(self.number):
                if self.board[x][y] != player:
                    win = False
                    continue
            if win == True:
                return win
        return win

    def col_win(self, player): 
        for x in range(self.number): 
            win = True
            
            for y in range(self.number): 
                if self.board[y][x] != player: 
                    win = False
                    continue
                    
            if win == True: 
                return(win) 
        return(win) 

    def diag_win(self, player): 
        win = True
        
        for x in range(self.number): 
            if self.board[x][x] != player: 
                win = False
        return(win)
    
    def check(self):
        dictionary = {"X":1,"O":2}
        for i in dictionary.keys():
            if self.row_win(i) or self.col_win(i) or self.diag_win(i):
                print("The winner is: player",dictionary[i])

    def move(self, row, col, player):
        os.system('cls')
        if player == 1:
            self.board[row][col] = "X"
        if player == 2:
            self.board[row][col] = "O"
        TicTacToe.check(self)
        self.draw_board()

    # Fill this in.
if __name__ == "__main__":
    board = TicTacToe(3)
    board.move(0, 0, 1)
    board.move(0, 2, 2)
    board.move(2, 2, 1)
    board.move(1, 1, 2)
    board.move(2, 0, 1)
    board.move(1, 0, 2)
    board.move(2, 1, 1)
